#include <stdio.h>
/* TIMING CODE BEGIN (We need the following lines to take the timings.) */
#include <stdlib.h>
#include <math.h>
#include <time.h>
clock_t startm, stopm;
#define RUNS 1
#define START if ( (startm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define STOP if ( (stopm = clock()) == -1) {printf("Error calling clock");exit(1);}
#define PRINTTIME printf( "%8.5f seconds used .", (((double) stopm-startm)/CLOCKS_PER_SEC/RUNS));
/* TIMING CODE END */

double f(double x);			/*Function Prototypes*/
double pi(long n);


int main(void) {
  START;               /* Timing measurement starts here */

  printf("Pi = %.8f\n ", pi(10000000));
  /* Code to be written by student, calling functions from here is fine
     if desired */
  
  STOP;                /* Timing measurement stops here */
  PRINTTIME;           /* Print timing results */
  printf("\n");
  return 0;
}


double f(double x){
	double num;
	num = pow((1-pow(x,2)), 0.5);
	return num;
}

double pi(long n){
	int a=-1, b=1, i;
	float h;
	double s, pi, x;

	h = (float)(b-a)/n;
	s = 0.5*f(a) +0.5*f(b);

	for(i=0; i<n; i++){
		x = a+i*h;
		s = s+f(x);
	}
	pi = s*h*2;
	return pi;
}
