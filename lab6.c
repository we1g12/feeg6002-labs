/* Laboratory 6, SESG6025, 2013/2014, Template */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

/* Function void rstrip(char s[])
modifies the string s: if at the end of the string s there are one or more spaces,
then remove these from the string.

The name rstrip stands for Right STRIP, trying to indicate that spaces at the 'right'
end of the string should be removed.
*/

void lstrip(char s[]) {
    char * p = s;
    int l = strlen(p);

    while(isspace(p[l - 1])) p[--l] = 0;
    while(* p && isspace(* p)) ++p, --l;

    memmove(s, p, l + 1);
}



int main(void) {
  char test1[] = "     Hello World";

  printf("Original string reads  : |%s|\n", test1);
  lstrip(test1);
  printf("r-stripped string reads: |%s|\n", test1);

  return 0;
}
